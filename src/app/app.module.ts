import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {HttpClientModule} from '@angular/common/http';

import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import {RouterModule} from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { UsersListComponent } from './users-list/users-list.component';
import {PaginationModule} from 'ngx-bootstrap/pagination';
import {FormsModule} from '@angular/forms';
import { UserCardComponent } from './user-card/user-card.component';
import { UserDetailsComponent } from './user-details/user-details.component';
import { SearchUserListPipe } from './search-user-list.pipe';
import {ContentLoaderModule} from '@netbasal/ngx-content-loader';
import { NgxPaginationModule } from 'ngx-pagination';
import {LoadingBarHttpClientModule} from '@ngx-loading-bar/http-client';





@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    UsersListComponent,
    UserCardComponent,
    UserDetailsComponent,
    SearchUserListPipe
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule,
    AppRoutingModule,
    PaginationModule,
    FormsModule,
    ContentLoaderModule,
    NgxPaginationModule,
    LoadingBarHttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
