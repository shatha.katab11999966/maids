import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import {catchError, shareReplay} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private REST_API_SERVER = 'https://reqres.in/api/users/';
  constructor(private httpClient: HttpClient) { }


  handleError(error: HttpErrorResponse) {
    let errorMessage = 'Unknown error!';
    if (error.error instanceof ErrorEvent) {
      // Client-side errors
      errorMessage = `Error: ${error.error.message}`;
    } else {
      // Server-side errors
      errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(errorMessage);
  }

  public getUsersList(page): Observable<any>{
    return this.httpClient.get<any>(this.REST_API_SERVER + `?page=${page}`).pipe(catchError(this.handleError), shareReplay(1));
  }

  public getUserDetails(userId): Observable<any>{
    return this.httpClient.get<any>(this.REST_API_SERVER + `${userId}`).pipe(catchError(this.handleError) , shareReplay(1));
  }
}
