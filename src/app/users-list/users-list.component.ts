import { Component, OnInit } from '@angular/core';
import {UsersService} from '../services/users.service';
import {SearchUserListPipe} from '../search-user-list.pipe';
import {Observable} from "rxjs";
import {map, tap} from "rxjs/operators";

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.css']
})
export class UsersListComponent implements OnInit {
  users: Observable<any[]>;
  asyncList: Observable<any[]>;
  p = 1;
  total: number;
  perPage: number;
  loading: boolean;
  userId;
  constructor(private service: UsersService) { }

  ngOnInit(): void {
    this.getUserListPerPage(1);
  }

  getUserListPerPage(page: number) {
    this.loading = true;
    this.users = this.service.getUsersList(page).pipe(
      tap(res => {
        this.total = res.total;
        this.perPage = res.per_page;
        this.p = page;
        this.loading = false;
      }),
      map(res => res.data)
    );
  }

}
