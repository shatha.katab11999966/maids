import { Component, OnInit } from '@angular/core';
import { ActivatedRoute  } from '@angular/router';
import {UsersService} from '../services/users.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-user-details',
  templateUrl: './user-details.component.html',
  styleUrls: ['./user-details.component.css']
})
export class UserDetailsComponent implements OnInit {

  userDetails;
  loaded;

  constructor(private activatedRoute: ActivatedRoute ,
              private service: UsersService ,
              private location: Location
              ) {
  }

  ngOnInit(): void {
      this.loaded = false;
      this.service.getUserDetails(this.activatedRoute.snapshot.params.id).subscribe((data: any = {}) => {
        this.loaded = true;
        this.userDetails = data.data;
      });
  }

  goBack(){
    this.location.back();
  }

}
