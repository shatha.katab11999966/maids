import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchUserList'
})
export class SearchUserListPipe implements PipeTransform {

  transform(value: any, input: any ): any {
    if (input) {
      return value.filter((item) => {
        return (JSON.stringify(item.id ) === input);
      });
     }
    else {
      return value;
    }
  }
}
