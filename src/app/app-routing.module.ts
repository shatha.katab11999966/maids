import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {UsersListComponent} from './users-list/users-list.component';
import {UserDetailsComponent} from './user-details/user-details.component';


const routes: Routes = [
  { path: 'users-list', component: UsersListComponent },
  { path: '', redirectTo: '/users-list', pathMatch: 'full'},
  { path: 'user-details/:id', component: UserDetailsComponent }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
